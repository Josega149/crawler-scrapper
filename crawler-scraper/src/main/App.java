package main;

import java.util.ArrayList;

public class App {
	
	
	public String name;
	public String numRating; 
	public double avgRating;
	public String category;
	public String recentCh; 
	public String fiveStarsR;
	public String fourStarsR;
	public String threeStarsR;
	public String twoStarsR;
	public String oneStarsR;
	public String description;
	public String isPaid;
	
	public App(String name, String numRating, String avgRating, String category,String isPaid, String recentCh, String fiveStarsR, String fourStarsR,String threeStarsR, String twoStarsR,String oneStarsR,
			String description) {
		this.name = name;
		this.numRating = numRating;
		this.avgRating = Double.parseDouble(avgRating.replace(',', '.'));
		this.recentCh = recentCh;
		this.fiveStarsR = fiveStarsR;
		this.fourStarsR = fourStarsR;
		this.threeStarsR = threeStarsR;
		this.twoStarsR = twoStarsR;
		this.oneStarsR = oneStarsR;
		this.description = description;
		this.isPaid = isPaid;
		this.category = category;
	}
}
