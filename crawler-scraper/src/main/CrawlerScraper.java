package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import jxl.*;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class CrawlerScraper {
	
	//seleccionar elementos con jsoup
	//https://jsoup.org/cookbook/extracting-data/selector-syntax
	
	public CrawlerScraper() throws Exception{
		
		//en hrefs quedan las urls de las app en detalle
		HashSet<String> hrefs = new HashSet<String>();
		
		String [] categorias = {	"HEALTH_AND_FITNESS",
									"EDUCATION",
									"ENTERTAINMENT",
									"MUSIC_AND_AUDIO",
									"PRODUCTIVITY",
									"TOOLS",
									"GAME_CASINO", 
									"GAME_ADVENTURE", 
									"TRAVEL_AND_LOCAL",
									"GAME_PUZZLE",
									"GAME_STRATEGY",
									"GAME_TRIVIA"};

		
		for(int i=0; i< categorias.length;i++){
			System.out.println("Recuperando info de la categoria: "+categorias[i]);
			String URL = "https://play.google.com/store/apps/category/"+categorias[i]+"/collection/topselling_paid";
			recuperarDadoMetodoDePago(URL, hrefs,categorias[i],"Paid");
			URL = "https://play.google.com/store/apps/category/"+categorias[i]+"/collection/topselling_free"; 
			recuperarDadoMetodoDePago(URL, hrefs,categorias[i],"Free");
		}
		
		System.out.println("\n Termina de recolectar urls de apps. Todas se encuentran en el hash. \n");
		//a este punto el hashset ya contiene todas las url de las mejores 10 apps por categor�a
		Document detailDoc = null;
		
		PriorityQueue<App> apps  = new PriorityQueue<App>(192, new AppComparator());
		System.out.println("Creando PriorityQueue de Apps. Tama�o: "+ hrefs.size());
		for(String url: hrefs){
			
			detailDoc = Jsoup.connect(url.split("--")[0]).timeout(0).get();
			String name = detailDoc.select("[class='id-app-title']").text();
			String numRating = detailDoc.select("[class='rating-count']").text();
			String avgRating = detailDoc.select("[class='score']").text();
			String recentCh = detailDoc.select("[class='recent-change']").text();
			String fiveStarsR = detailDoc.select("[class='rating-bar-container five']").text();
			String fourStarsR = detailDoc.select("[class='rating-bar-container four']").text();
			String threeStarsR = detailDoc.select("[class='rating-bar-container three']").text();
			String twoStarsR = detailDoc.select("[class='rating-bar-container two']").text();
			String oneStarsR = detailDoc.select("[class='rating-bar-container one']").text();
			String description = detailDoc.select("[itemprop='description']").text();
			App actual = new App(name, numRating,avgRating,url.split("--")[1],url.split("--")[2],recentCh,fiveStarsR,fourStarsR,threeStarsR,twoStarsR,oneStarsR,description);
			apps.add(actual);
		}
		System.out.println("Termina de crear la lista de apps con toda la informacion. \n Creando archivo excel...");
		//las apps estan sorteadas de mayor rating a menor
		WritableWorkbook workbook = Workbook.createWorkbook(new File("resultados.xls"));

		WritableSheet sheet = workbook.createSheet("Resumen Apps", 0);

		//						C  F   M
		Label label = new Label(0, 0, "Nombre"); 
		sheet.addCell(label); 
		label = new Label(1, 0, "Descripcion"); 
		sheet.addCell(label);
		label = new Label(2, 0, "Categoria"); 
		sheet.addCell(label);
		label = new Label(3, 0, "Es paga?"); 
		sheet.addCell(label);
		label = new Label(4, 0, "Num Ratings"); 
		sheet.addCell(label);
		label = new Label(5, 0, "Avg Rating"); 
		sheet.addCell(label);
		label = new Label(6, 0, "Recent Changes"); 
		sheet.addCell(label);
		label = new Label(7, 0, "Ratings 5 estrellas"); 
		sheet.addCell(label);
		label = new Label(8, 0, "Ratings 4 estrellas"); 
		sheet.addCell(label);
		label = new Label(9, 0, "Ratings 3 estrellas"); 
		sheet.addCell(label);
		label = new Label(10,0, "Ratings 2 estrellas"); 
		sheet.addCell(label);
		label = new Label(11, 0, "Ratings 1 estrellas"); 
		sheet.addCell(label);
		

		/**Number number = new Number(3, 4, 3.1459); 
		sheet.addCell(number);*/
		int fila = 1;
		while(apps.size() >0){
			App actual = apps.poll();
			
			label = new Label( 0, fila, actual.name); 
			sheet.addCell(label); 
			label = new Label(1, fila, actual.description); 
			sheet.addCell(label);
			label = new Label(2, fila, actual.category); 
			sheet.addCell(label);
			label = new Label(3, fila, actual.isPaid); 
			sheet.addCell(label);
			label = new Label(4, fila, actual.numRating); 
			sheet.addCell(label);
			label = new Label(5, fila, actual.avgRating+""); 
			sheet.addCell(label);
			label = new Label(6, fila, actual.recentCh); 
			sheet.addCell(label);
			label = new Label(7, fila, actual.fiveStarsR); 
			sheet.addCell(label);
			label = new Label(8, fila, actual.fourStarsR); 
			sheet.addCell(label);
			label = new Label(9, fila, actual.threeStarsR); 
			sheet.addCell(label);
			label = new Label(10, fila, actual.twoStarsR); 
			sheet.addCell(label);
			label = new Label(11, fila, actual.oneStarsR); 
			sheet.addCell(label);
			
			fila++;
		}
		workbook.write();
        workbook.close();
		System.out.println("El archivo excel esta listo.");
	}
	
	public void recuperarDadoMetodoDePago(String urlMetodoPago, HashSet<String> hrefs, String category,String paid) throws Exception{

			// En doc esta el html de la pag.
			Document doc = Jsoup.connect(urlMetodoPago).timeout(0).get();
			//System.out.println(doc);
			
			// en anchors quedan todos los tag completos que tengan como clase "card.."
			Elements anchors = doc.getElementsByClass("card-click-target");
			int i =0;
			for(Element el : anchors){
				if(i > 30){
					break;
				}
				//recupera todos los hrefs de los tag con clase card...
				hrefs.add("https://play.google.com/"+el.attr("href").toString() + "--"+category + "--"+paid);
				i++;
			}
	}
	
	private class AppComparator implements Comparator <App>{

		@Override
		public int compare(App app1, App app2) {
			return Double.compare(app1.avgRating, app2.avgRating)*-1;
		}
		
	}
	
	public static void main(String[] args) {
		try {
			CrawlerScraper cs = new CrawlerScraper();
		} catch (Exception e) {
			System.out.println("ERROOR");
			e.printStackTrace();
		}
	}

}
